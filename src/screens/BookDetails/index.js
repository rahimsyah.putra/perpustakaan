import React , { useState, useEffect }from 'react'
import { Alert, StyleSheet, StatusBar, View, ScrollView, Text, TouchableOpacity } from 'react-native';

import firestore from '@react-native-firebase/firestore';
import { Header, Icon, Card, Button } from 'react-native-elements';


const BookDetails = ({navigation, route}) => {

    const [ loading, setLoading ] = useState(true);
    const [ book, setBook ] = useState({});
    const [ key, setKey] = useState('');

    const ref = firestore().collection('books');

    useEffect(() => {
        
        ref.doc(route.params.bookkey).get().then((res) => {

            const list = [];
            if (res.exists) {
                const { data } = res.data();

                setBook(data)
                setKey(res.id)

              } else {
                console.log("Document does not exist!");
              }
          });
    }, [loading]);

    const deleteData =( key) => {

        firestore().collection('books').doc(key).delete().then(() => {
          console.log("Document successfully deleted!");
          setLoading(false);
          navigation.navigate('Home');

        }).catch((error) => {
          console.error("Error removing document: ", error);
          setLoading(false);
        });
    };

    const openTwoButtonAlert=()=>{
      Alert.alert(
        'Remove Data',
        'Are you sure?',
        [
          {text: 'Yes', onPress: () => deleteData(key)},
          {text: 'No', onPress: () => console.log('No item was removed'), style: 'cancel'},
        ],
        { 
          cancelable: true 
        }
      );
    }

    return(
        <>
            <StatusBar barStyle="light-content" backgroundColor="#D6D6D6" />
            <Header
                leftComponent={
                <TouchableOpacity
                    onPress={() => {
                    navigation.goBack();
                    }}
                >
                    <Icon
                    name="arrow-left"
                    type="simple-line-icon"
                    color="#FFFFFF"
                    containerStyle={{ marginLeft: "10%", elevation: 5 }}
                    size={25}
                    />
                </TouchableOpacity>
                }
                centerComponent={<Text style={styles.textHeader}>Detail</Text>}
                containerStyle={{
                backgroundColor: "#2892D7",
                }}
            />

            <ScrollView>
      <Card style={styles.container}>
        <View style={styles.subContainer}>
          <View>
            <Text style={styles.textJudul}>{book.title}</Text>

            <Text style={styles.textDesc}>Kode Buku    : {book.id} </Text>
            <Text style={styles.textDesc}>Penerbit        : {book.publisher}</Text>
            <Text style={styles.textDesc}>Tahun Terbit : {book.year}</Text>
            <Text style={styles.textDesc}>Pengarang    : {book.author}</Text>
          </View>
        </View>
        <View style={styles.detailButton}>
          <Button
            large
            backgroundColor={'#CCCCCC'}
            leftIcon={{name: 'edit'}}
            title='Edit'
            onPress={() => {
              navigation.navigate('EditData', {
                bookkey: key
              });
            }} />
        </View>
        <View style={styles.detailButton}>
          <Button
            large
            backgroundColor={'#999999'}
            color={'#FFFFFF'}
            leftIcon={{name: 'delete'}}
            title='Delete'
            onPress={() => openTwoButtonAlert()} />
        </View>
      </Card>
    </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 20,
      
    },
    subContainer: {
      flex: 1,
      paddingBottom: 20,
      borderBottomWidth: 2,
      borderBottomColor: '#CCCCCC',
      
    },
    activity: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'
    },
    textHeader: {
      fontSize: 20,
      letterSpacing: 2,
      color: "#FFFFFF",
      fontFamily: "Nunito",
      fontWeight: "400",
    },
    detailButton: {
      marginTop: 10
    },
    textJudul:{
      color: 'black',
      fontSize: 24,
      fontWeight:'bold',
      marginBottom: 10,
    },
    textTahun:{
      color: 'black',
      fontSize: 18,
    },
    textDesc:{
      color: 'black',
      fontSize: 16,
    }
})
export default BookDetails;