import React , { Component }from 'react'
import { StyleSheet, StatusBar, View, ScrollView, TouchableOpacity, Text } from 'react-native';

import firestore from '@react-native-firebase/firestore';
import { TextInput, Button } from 'react-native-paper';
import { Header, Icon } from "react-native-elements";

class EditData extends Component {

  constructor() {
    super();
    this.state = {
      data: {
        publisher: '',
        author: '',
        id: '',
        title: '',
        year: '',
      },
    };
  }
    
    componentDidMount() {
        
      firestore().collection('books').doc(this.props.route.params.bookkey).get().then((res) => {
            if (res.exists) {
                const { data } = res.data();

                this.setState({
                  data: {
                    key: res.id,
                    publisher: data.publisher,
                    author: data.author,
                    id: data.id,
                    title: data.title,
                    year: data.year,
                  }
                });
                console.log(data);
              } else {
                console.log("Document does not exist!");
              }
          });
    }

    componentWillUnmount() {
        
       firestore().collection('books').doc(this.props.route.params.bookkey).get().then((res) => {
        if (res.exists) {
            const { data } = res.data();

            this.setState({
              data: {
                key: res.id,
                publisher: data.publisher,
                author: data.author,
                id: data.id,
                title: data.title,
                year: data.year,
              }
            });
            console.log(data);
          } else {
            console.log("Document does not exist!");
          }
      });
    }

    updateTextInput = (text, prop) =>{

      const state = this.state.data;
      state[prop] = text;
      this.setState(state);
    }

    updateData() {

      const updateRef = firestore().collection('books').doc(this.state.key);
      updateRef.set({
        data: {
          publisher: this.state.data.publisher,
          author: this.state.data.author,
          id: this.state.data.id,
          title: this.state.data.title,
          year: this.state.data.year,
        }
      }).then((docRef) => {
        this.setState({
          data:{
            publisher: '',
            author: '',
            id: '',
            title: '',
            year: '',
          }
        });
        this.props.navigation.navigate('Home');
      }).catch((err) => {
        console.log("error update : ", err)
      })
    }

    
    
    render(){

      return(
        <>
            <StatusBar barStyle="light-content" backgroundColor="#D6D6D6" />
            <Header
                leftComponent={
                <TouchableOpacity
                    onPress={() => {
                    this.props.navigation.goBack();
                    }}
                >
                    <Icon
                    name="arrow-left"
                    type="simple-line-icon"
                    color="#FFFFFF"
                    containerStyle={{ marginLeft: "10%", elevation: 5 }}
                    size={25}
                    />
                </TouchableOpacity>
                }
                centerComponent={<Text style={styles.textHeader}>Edit Data</Text>}
                containerStyle={{
                backgroundColor: "#2892D7",
                }}
            />

            <ScrollView style={styles.container}>
            <View style={styles.subContainer}>
              <TextInput
                  label={'Publiser'}
                  placeholder={'Publisher'}
                  value={this.state.data.publisher}
                  onChangeText={(text) => this.updateTextInput(text, 'publisher')}
              />
            </View>
            <View style={styles.subContainer}>
              <TextInput
                  label={'Author'}
                  placeholder={'Author'}
                  value={this.state.data.author}
                  onChangeText={(text) => this.updateTextInput(text, 'author')}
              />
            </View>
            <View style={styles.subContainer}>
              <TextInput
                  label={'Title'}
                  placeholder={'Title'}
                  value={this.state.data.title}
                  onChangeText={(text) => this.updateTextInput(text, 'title')}
              />
            </View>
            <View style={styles.subContainer}>
              <TextInput
                  label={'Book Code'}
                  placeholder={'Book Code'}
                  value={this.state.data.id}
                  onChangeText={(text) => this.updateTextInput(text, 'id')}
              />
            </View>
            <View style={styles.subContainer}>
              <TextInput
                  label={'Release Year'}
                  placeholder={'Release'}
                  value={this.state.data.year}
                  onChangeText={(text) => this.updateTextInput(text, 'year')}
              />
            </View>
            <View>
              <Button
                mode="contained" 
                style={styles.button}
                onPress={() => this.updateData()} >
                  UPDATE
                </Button>
            </View> 
          </ScrollView>
        </>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textHeader: {
    fontSize: 20,
    letterSpacing: 2,
    color: "#FFFFFF",
    fontFamily: "Nunito",
    fontWeight: "400",
  },
  button:{
    padding: 10,
    backgroundColor: '#2892D7',
    borderRadius: 10
  }
})

export default EditData;