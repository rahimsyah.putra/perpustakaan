import React , { useState, useEffect }from 'react'
import { StyleSheet, StatusBar, ScrollView, Text, TouchableOpacity } from 'react-native';

import firestore from '@react-native-firebase/firestore';

import { Header, ListItem, Icon } from 'react-native-elements'

const Home =({ navigation })=>{
    
    const [ loading, setLoading ] = useState(true);
    const [ books, setBooks ] = useState([]);

    
    const ref = firestore().collection('books').orderBy('data.year', 'desc');

    useEffect(() =>{
        return ref.onSnapshot((querySnapshot) => {
            const list = [];
            querySnapshot.forEach(doc => {
                const { data } = doc.data();
                list.push({
                    key: doc.id,
                    data
                });
            });
                
            setBooks(list);

            if(loading) {
                setLoading(false);
            }
        });
    }, [loading]);

    if(loading){
        return null;
    }

    return(

        <>
            <StatusBar barStyle="light-content" backgroundColor="#D6D6D6" />
            <Header
                
                centerComponent={<Text style={styles.textHeader}>LIBRARY</Text>}
                rightComponent={
                    <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('AddData');
                    }}
                >
                    <Icon
                        name="plus-circle"
                        type="font-awesome"
                        color="#FFFFFF"
                        containerStyle={{ marginRight: "10%"}}
                        size={25}
                    />
                </TouchableOpacity>
                }
                containerStyle={{
                backgroundColor: "#2892D7",
                }}
            />
            <ScrollView style={styles.container}>
                
                <Text style={styles.textIntro}>List of Books</Text>

            {
                books.map((item, i) => {
                return (

                    <ListItem 
                        key={i} 
                        bottomDivider
                        style={styles.box} 
                        onPress={() => { navigation.navigate('BookDetails', {
                          bookkey: item.key
                        });
                      }}>
                        <Icon name='library-books' type="MaterialIcons" />
                        <ListItem.Content >
                            <ListItem.Title >{item.data.title}</ListItem.Title>
                            <ListItem.Subtitle>{item.data.year}</ListItem.Subtitle>
                        </ListItem.Content>
                        <ListItem.Chevron color="black" size={30} />
                    </ListItem>
                );
                })
            }

            </ScrollView>
            
        </>
    )

}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    textHeader: {
        fontSize: 20,
        letterSpacing: 2,
        color: "#FFFFFF",
        fontFamily: "Nunito",
        fontWeight: "400",
    },
    textIntro:{
        padding: 20,
        fontSize: 18,
        color: 'black'
    },
    box:{
        padding:6,
        
    }
})

export default Home;