import React , { useState, useEffect }from 'react'
import { StyleSheet, StatusBar, TouchableOpacity, Text } from 'react-native';

import firestore from '@react-native-firebase/firestore';
import { TextInput, Button } from 'react-native-paper';
import { Header, Icon } from "react-native-elements";

const CreateData = ({navigation}) => {

    const [ publisher, setPubliser ] = useState('');
    const [ author, setAuthor] = useState('');
    const [ id, setId ] = useState('');
    const [ title, setTitle ] = useState('');
    const [ year, setYear] = useState('');

    let data = {
        publisher: publisher,
        author: author, 
        id: id,
        title: title,
        year: year
    }
    const ref = firestore().collection('books');

    async function addData() {
        await ref.add({
            data 
        });
        setPubliser('');
        setAuthor('');
        setId('');
        setTitle('');
        setYear('');

        navigation.navigate('Home');
    }

    return(

        <>
            <StatusBar barStyle="light-content" backgroundColor="#D6D6D6" />
            <Header
                leftComponent={
                <TouchableOpacity
                    onPress={() => {
                    navigation.goBack();
                    }}
                >
                    <Icon
                    name="arrow-left"
                    type="simple-line-icon"
                    color="#FFFFFF"
                    containerStyle={{ marginLeft: "10%", elevation: 5 }}
                    size={25}
                    />
                </TouchableOpacity>
                }
                centerComponent={<Text style={styles.textHeader}>Add Data</Text>}
                containerStyle={{
                backgroundColor: "#2892D7",
                }}
            />

            <TextInput style={styles.textInput} label={'Book Code'} value={id} onChangeText={setId} />
            <TextInput style={styles.textInput} label={'Title'} value={title} onChangeText={setTitle} />
            <TextInput style={styles.textInput} label={'Publiser'} value={publisher} onChangeText={setPubliser} />
            <TextInput style={styles.textInput} label={'Year'} value={year} onChangeText={setYear} />
            <TextInput style={styles.textInput} label={'Author'} value={author} onChangeText={setAuthor} />
            
            

            <Button 
                mode="contained" 
                style={styles.button}
                onPress={() => addData()}
                >Add Data
            </Button>
        </>
    )
}

const styles = StyleSheet.create({
    textHeader: {
        fontSize: 18,
        letterSpacing: 2,
        color: "#FFFFFF",
        fontFamily: "Nunito",
        fontWeight: "400",
    },
    textInput:{
        margin: 15,
    },
    button:{
        margin: 10,
        padding: 10,
        backgroundColor: '#2892D7',
        borderRadius: 10
    }
})

export default CreateData;