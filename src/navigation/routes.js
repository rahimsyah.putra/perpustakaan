import React, { UseState, UseEffect } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator }from '@react-navigation/stack';


import Home from '../screens/Home';
import CreateData from '../screens/CreateData';
import BookDetails from '../screens/BookDetails';
import EditData from '../screens/EditData';

const Stack = createStackNavigator()

const MainNavigation =()=> ( 
    <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="AddData" component={CreateData} options={{ headerShown: false }} />
        <Stack.Screen name="BookDetails" component={BookDetails} options={{ headerShown: false }} />
        <Stack.Screen name="EditData" component={EditData} options={{ headerShown: false }} />
    </Stack.Navigator>
) 

const AppNavigation = () => {

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation