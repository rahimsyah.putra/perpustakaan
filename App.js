import React from 'react';
import AppNavigation from './src/navigation/routes';

const App = () => {
  return (
    <>
      <AppNavigation />
    </>
  );
};


export default App;
